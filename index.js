'use strict';

class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name (){
        return this._name;
    }

    set name(value){
        this._name = value;
    }

    get age (){
        return this._age;
    }

    set age(value){
        this._age = value;
    }

    get salary (){
        return this._salary;
    }

    set salary(value){
        this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang;
    }

    get lang(){
        return this._lang;
    }

    set lang(value){
        this._lang = value;
    }

    get salary(){
        return this._salary *= 3;
    }
};

let p1 = new Programmer("John" , 45, 1200, ["EN" , "UA"]);
p1.salary;
let p2 = new Programmer("Anna" , 30, 1100, ["EN" , "UA" , "DE"]);
p2.salary;

console.log(p1);
console.log(p2);